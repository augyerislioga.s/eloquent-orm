@extends('layout.master')

@section('judul')
Tambah data Genre
@endsection

@section('isi')

<div>
        <form action="/genre" method="POST">
            @csrf
            <div class="form-group">
                <label>Nama genre</label>
                <input type="text" class="form-control" name="nama" placeholder="Masukkan Title">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>
@endsection