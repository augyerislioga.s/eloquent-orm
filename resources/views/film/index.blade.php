@extends('layout.master')

@section('judul')
Judul Film
@endsection

@section('isi')
<a href="/film/create" class="btn btn-primary">Tambah Film</a>
<div class="row">
    @foreach ($film as $value)
        
    
    <div class="col-4">
        <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="{{asset('upload/film/'.$value->poster)}}" alt="Card image cap">
            <div class="card-body">
              <h5 class="card-title mb-3">{{$value->judul}} ({{$value->tahun}})</h5>
              <p class="card-text">{{ Str::limit($value->ringkasan, 200) }}</p>
              <a href="#" class="btn btn-primary">Go somewhere</a>
            </div>
            @endforeach`
  </div>

@endsection