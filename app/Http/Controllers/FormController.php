<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function form(){
        return view('pages.form');
    }

    public function kirim(Request $request){
        $firstname = $request->firstname;
        $lastname = $request->lastname;
        return view('pages.welcome', compact('firstname', 'lastname'));
    }
}
